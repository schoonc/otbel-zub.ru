const screens = ['main', 'price', 'form']
const phone = {
  code: '+7 (495)',
  number: '999-99-99'
}

export const state = () => ({
  activeScreen: screens[0],
  screens,
  entering: false,
  phone,
  phoneSimple: `${phone.code}${phone.number}`.replace(/[^\d]/g, ''),
  mobile: null
})

export const mutations = {
  SET_ACTIVE_SCREEN(state, screen) {
    state.activeScreen = screen
  },
  SET_ENTERING(state, entering) {
    state.entering = entering
  },
  SET_MOBILE(state, mobile) {
    state.mobile = mobile
  }
}

export const actions = {
  SET_PREV_SCREEN({ commit, state }) {
    const index = state.screens.indexOf(state.activeScreen)
    if (index !== 0) commit('SET_ACTIVE_SCREEN', state.screens[index - 1])
  },
  SET_NEXT_SCREEN({ commit, state }) {
    const index = state.screens.indexOf(state.activeScreen)
    if (index !== state.screens.length - 1)
      commit('SET_ACTIVE_SCREEN', state.screens[index + 1])
  }
}
